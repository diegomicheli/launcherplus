package movilplus.com.launcherplus

import org.junit.Test

import movilplus.com.launcherplus.models.AppDetail

import org.junit.Assert.*

class TestAppDetail {

    @Test
    @Throws(Exception::class)
    fun getAppFullName_returnPackagePlusLabel() {
        val appDetail = AppDetail("LauncherPlus", "com.movilplus.launcherplus", null, false)

        assertEquals("com.movilplus.launcherplus.LauncherPlus", appDetail.fullName)
    }

}