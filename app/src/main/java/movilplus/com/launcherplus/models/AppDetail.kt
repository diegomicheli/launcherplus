package movilplus.com.launcherplus.models

import android.graphics.drawable.Drawable

data class AppDetail(var label: CharSequence, var name: CharSequence, var icon: Drawable?, var isSelected: Boolean) {

    var fullName: String = "$name.$label"

}
