package movilplus.com.launcherplus

import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Base64
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import movilplus.com.launcherplus.models.AppDetail
import movilplus.com.launcherplus.preferences.Preferences
import movilplus.com.launcherplus.services.Apps


class MainActivity : BaseActivity() {

    internal var mAppsGridView: GridView? = null
    internal var logoImageView: ImageView? = null
    internal var mVisibleApps: ArrayList<AppDetail> = ArrayList()

    lateinit internal var cancelAction: TextView
    lateinit internal var finishAction: TextView
    lateinit internal var toggleSelectionAction: TextView
    lateinit internal var settingsIcon: ImageView
    lateinit internal var menuBar: LinearLayout

    internal var isSelectionMode: Boolean = false

    val OVERLAY_PERMISSION_REQUEST_CODE = 1553

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.setUpViews()
        this.loadApps()
        this.loadListView()
        this.addOnItemClickListener()
        supportActionBar?.hide()
        checkDrawOverlayPermission()
    }

    override fun onResume() {
        super.onResume()
        loadLogo()
        Apps.isSettingsOpened = false
    }

    fun checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            preventStatusBarExpansion(this)
            return
        }

        /** check if we already  have permission to draw over other apps  */
        if (!Settings.canDrawOverlays(this)) {
            /** if not construct intent to request permission  */
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + packageName))
            /** request permission via start activity for result  */
            startActivityForResult(intent, OVERLAY_PERMISSION_REQUEST_CODE)
        } else {
            preventStatusBarExpansion(this)
        }
    }

    private fun setUpViews() {
        mAppsGridView = findViewById(R.id.appsGridView) as GridView
        logoImageView = findViewById(R.id.logo) as ImageView
        cancelAction = findViewById(R.id.cancel_action) as TextView
        finishAction = findViewById(R.id.finish_action) as TextView
        toggleSelectionAction = findViewById(R.id.toggle_selection_action) as TextView
        settingsIcon = findViewById(R.id.settingsIcon) as ImageView
        menuBar = findViewById(R.id.menuBar) as LinearLayout
    }

    private fun loadLogo() {
        val preferences = getSharedPreferences(Preferences.APP_SETTINGS.toString(), Context.MODE_PRIVATE)
        val logoPath = preferences.getString(Preferences.LOGO.toString(), null)

        if (logoPath != null) {
            val decodedString = Base64.decode(logoPath, Base64.DEFAULT)
            val bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            if (bitmap != null) {
                logoImageView?.setImageBitmap(bitmap)
            }
        }
    }

    private fun loadApps(loadAll: Boolean = false) {
        val manager = packageManager
        this.mVisibleApps = ArrayList<AppDetail>()

        val selectedAppsNames = getSelectedAppsNames()
        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        getAllApps().map { AppDetail(it.loadLabel(manager), it.activityInfo.packageName, it.activityInfo.loadIcon(manager), selectedAppsNames.contains(it.activityInfo.packageName)) }
                .forEach {

                    if (loadAll || it.isSelected) {
                        mVisibleApps.add(it)
                    }
                }
    }

    private fun loadListView() {

        val adapter = object : ArrayAdapter<AppDetail>(this,
                R.layout.list_item,
                mVisibleApps) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

                val convertView = layoutInflater.inflate(R.layout.list_item, null)

                val appIcon = convertView?.findViewById(R.id.item_app_icon) as ImageView
                val appLabel = convertView.findViewById(R.id.item_app_label) as TextView
                val visibleApp = getItem(position)

                appIcon.setImageDrawable(visibleApp.icon)
                appLabel.text = visibleApp.label

                if (this@MainActivity.isSelectionMode && visibleApp.isSelected) {
                    convertView.setBackgroundDrawable(resources.getDrawable(R.drawable.app_background_selected))
                }

                return convertView
            }
        }

        mAppsGridView?.adapter = adapter
    }

    private fun addOnItemClickListener() {
        mAppsGridView?.onItemClickListener = AdapterView.OnItemClickListener(appClicked())
    }

    private fun appClicked(): (AdapterView<*>, View, Int, Long) -> Unit {
        return { av, view, pos, id ->

            if (!isSelectionMode) {
                val i = packageManager.getLaunchIntentForPackage(mVisibleApps[pos].name.toString())
                this@MainActivity.startActivity(i)
            } else {

                if (!mVisibleApps[pos].isSelected) {
                    mVisibleApps[pos].isSelected = true
                    view.setBackgroundDrawable(resources.getDrawable(R.drawable.app_background_selected))
                } else {
                    mVisibleApps[pos].isSelected = false
                    view.setBackgroundDrawable(resources.getDrawable(R.drawable.app_background_normal))
                }

            }

        }
    }

    override fun onBackPressed() {
        // No call to super.onBackPressed, since this would quit the launcher.
    }

    fun openSettings(view: View) {
        showSecurityDialog({
            startActivityForResult(Intent(this, SettingsActivity::class.java), SELECT_APPS)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == SELECT_APPS) {
            this.loadApps(true)
            this.loadListView()
            showActions()
            isSelectionMode = true

            val toggleSelectionAction = findViewById(R.id.toggle_selection_action) as TextView
            val appsQty = this.mVisibleApps.count()
            toggleSelectionAction.text = if (this.mVisibleApps.count { it.isSelected } == appsQty) getString(R.string.action_deselect_all) else getString(R.string.action_select_all)
        } else if (requestCode == OVERLAY_PERMISSION_REQUEST_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    preventStatusBarExpansion(this)
                }
            }
            else {
                preventStatusBarExpansion(this)
            }
        }
    }

    fun showActions() {
        cancelAction.visibility = View.VISIBLE
        toggleSelectionAction.visibility = View.VISIBLE
        finishAction.visibility = View.VISIBLE
        settingsIcon.visibility = View.GONE
        menuBar.gravity = Gravity.CENTER
        logoImageView?.visibility = View.GONE
    }

    fun cancelSelection() {
        this.loadApps()
        this.loadListView()
        dismissActionsMenu()
        isSelectionMode = false
    }

    private fun dismissActionsMenu() {
        cancelAction.visibility = View.GONE
        toggleSelectionAction.visibility = View.GONE
        finishAction.visibility = View.GONE
        settingsIcon.visibility = View.VISIBLE
        menuBar.gravity = Gravity.RIGHT
        logoImageView?.visibility = View.VISIBLE
    }


    fun doAction(view: View) {

        when (view.id) {
            R.id.cancel_action -> {
                cancelSelection()
            }
            R.id.toggle_selection_action -> {
                toggleAppsSelection()
            }
            R.id.finish_action -> {
                saveAppsSelection()
            }
        }

    }

    private fun toggleAppsSelection() {

        if (toggleSelectionAction.text == getString(R.string.action_select_all)) {
            this.mVisibleApps.forEach {
                it.isSelected = true
            }
            toggleSelectionAction.text = getString(R.string.action_deselect_all)
        } else {
            this.mVisibleApps.forEach {
                it.isSelected = false
            }
            toggleSelectionAction.text = getString(R.string.action_select_all)
        }

        this.loadListView()
    }

    private fun saveAppsSelection() {

        val selectedApps = mVisibleApps.filter { it.isSelected }
                .map { it.name.toString() }
                .toSet()

        val preferences = getSharedPreferences(Preferences.APP_SETTINGS.toString(), Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putStringSet(Preferences.SELECTED_APPS.toString(), selectedApps)
        editor.commit()

        this.loadApps()
        this.loadListView()
        dismissActionsMenu()
        isSelectionMode = false
        Toast.makeText(this, getString(R.string.saved_apps_selection), Toast.LENGTH_SHORT).show()
    }

    private fun getSelectedAppsNames(): MutableSet<String> {
        val preferences = getSharedPreferences(Preferences.APP_SETTINGS.toString(), Context.MODE_PRIVATE)
        return preferences.getStringSet(Preferences.SELECTED_APPS.toString(), getAllApps().map { it.activityInfo.packageName }.toMutableSet())
    }

    private fun getAllApps(): MutableSet<ResolveInfo> {
        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)
        return packageManager.queryIntentActivities(i, 0).filter { it.activityInfo.packageName != this.packageName }.toMutableSet()
    }
}