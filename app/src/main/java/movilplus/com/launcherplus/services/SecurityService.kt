package movilplus.com.launcherplus.services

import android.app.ActivityManager
import android.app.IntentService
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import movilplus.com.launcherplus.SecurityActivity
import java.util.*
import kotlin.concurrent.thread

object Apps {
    var isSettingsOpened = false
    val SETTINGS_APP = "com.android.settings"
}

class SecurityService : IntentService("SecurityService") {


    override fun onHandleIntent(intent: Intent?) {

        thread {
            while (true) {

                val appName = getTopAppName(this)

                if (Apps.SETTINGS_APP == appName && !Apps.isSettingsOpened) {
                    val i = Intent(this, SecurityActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                    Apps.isSettingsOpened = true
                }

                Thread.sleep(500)
            }
        }

    }

    fun getTopAppName(context: Context): String {
        val mActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        var strName = ""
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                strName = getLollipopFGAppPackageName(context)
            } else {
                strName = mActivityManager.getRunningTasks(1)[0].topActivity.className
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return strName
    }

    private fun getLollipopFGAppPackageName(ctx: Context): String {

        try {
            val usageStatsManager = ctx.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
            val milliSecs = (60 * 1000).toLong()
            val date = Date()
            val queryUsageStats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, date.time - milliSecs, date.time)
            if (queryUsageStats.size > 0) {
                Log.i("LPU", "queryUsageStats size: " + queryUsageStats.size)
            }
            var recentTime: Long = 0
            var recentPkg = ""
            for (i in queryUsageStats.indices) {
                val stats = queryUsageStats[i]
                if (i == 0 && "org.pervacio.pvadiag" != stats.packageName) {
                    Log.i("LPU", "PackageName: " + stats.packageName + " " + stats.lastTimeStamp)
                }
                if (stats.lastTimeStamp > recentTime) {
                    recentTime = stats.lastTimeStamp
                    recentPkg = stats.packageName
                }
            }
            return recentPkg
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

}