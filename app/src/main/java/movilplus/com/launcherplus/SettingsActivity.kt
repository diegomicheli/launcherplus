package movilplus.com.launcherplus

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.*
import movilplus.com.launcherplus.preferences.Preferences
import java.io.ByteArrayOutputStream


val SELECT_APPS: Int = 10001

class SettingsActivity : BaseActivity() {


    internal var settingsLv: ListView? = null
    internal var settingsItems: ArrayList<String>? = ArrayList()

    private val PICK_IMAGE_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        settingsLv = findViewById(R.id.settingsLV) as ListView
        setUpActionBar()
        settingsItems?.addAll(resources.getStringArray(R.array.items_settings))
        loadListView()
        addOnItemClickListener()
        this.allowStatusBarExpansion(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.preventStatusBarExpansion(this)
    }

    private fun addOnItemClickListener() {
        settingsLv?.onItemClickListener = AdapterView.OnItemClickListener { av, v, pos, id ->

            when (pos) {
                0 -> {
                    setResult(SELECT_APPS)
                    finish()
                }
                1 -> selectLogo()
                2 -> showChangeSecurityPasswordDialog()
            }

        }
    }

    private fun selectLogo() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_logo_title)), PICK_IMAGE_REQUEST)
    }

    private fun setUpActionBar() {
        val bar = supportActionBar
        bar?.setDisplayShowTitleEnabled(false)
        bar?.setDisplayHomeAsUpEnabled(true)
        bar?.setBackgroundDrawable(ColorDrawable(R.color.opaquedBackgroundColor))
        bar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    private fun loadListView() {
        settingsLv?.adapter = ArrayAdapter<String>(this, R.layout.settings_list_item, R.id.textItem, settingsItems)
    }

    private fun showChangeSecurityPasswordDialog() {
        val builder = AlertDialog.Builder(this)
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_security, null);
        val passwordEdit = dialogView.findViewById(R.id.password) as EditText
        passwordEdit.hint = getString(R.string.new_password_hint)
        builder.setTitle(getString(R.string.change_password_dialog_title))
                .setView(dialogView)
                .setPositiveButton(getString(R.string.dialog_answer_ok), { dialog, id ->

                    val preferences = getSharedPreferences(Preferences.APP_SETTINGS.toString(), Context.MODE_PRIVATE)
                    val edit = preferences.edit()
                    val typedPassword = passwordEdit.text.toString();

                    if (typedPassword.isNotEmpty()) {
                        if (typedPassword.length >= 6) {
                            edit.putString(Preferences.SECURITY_PASSWORD.toString(), typedPassword)
                            edit.commit()
                            Toast.makeText(this, getString(R.string.password_changed_message), Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(this, getString(R.string.required_password_length_message), Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.password_required_message), Toast.LENGTH_SHORT).show()
                    }

                    dialog.dismiss()
                })
                .setNegativeButton(getString(R.string.dialog_answer_cancel), { dialog, id ->
                    dialog.cancel()
                }).create().show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {

            val uri = data.data

            val preferences = getSharedPreferences(Preferences.APP_SETTINGS.toString(), Context.MODE_PRIVATE)
            val edit = preferences.edit()

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)

            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
            edit.putString(Preferences.LOGO.toString(), encoded)
            edit.commit()

            finish()
        }
    }

}
