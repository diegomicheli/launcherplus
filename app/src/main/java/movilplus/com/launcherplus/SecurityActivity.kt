package movilplus.com.launcherplus

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import movilplus.com.launcherplus.services.Apps


class SecurityActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_security)
        supportActionBar?.hide()

        showSecurityDialog({
            finish()
            startActivity(Intent(Settings.ACTION_SETTINGS));
        }, {
            finish()
            killAppByPackage(Apps.SETTINGS_APP)
        })
    }

    override fun onBackPressed() {
        // No call to super.onBackPressed, since this would quit the launcher.
    }

    private fun killAppByPackage(packageToKill: String) {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        activityManager.killBackgroundProcesses(packageToKill)
    }
}
