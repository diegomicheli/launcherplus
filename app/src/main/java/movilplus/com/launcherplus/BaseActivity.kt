package movilplus.com.launcherplus

import android.app.AlertDialog
import android.content.Context
import android.graphics.PixelFormat
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.EditText
import android.widget.Toast
import movilplus.com.launcherplus.preferences.Preferences

object WindowViewTracker {
    var currentView: View? = null
}

open class BaseActivity : AppCompatActivity() {

    override fun onStart() {
        super.onStart()
        this.hideStatusBar()
    }

    protected fun hideStatusBar() {
        if (Build.VERSION.SDK_INT < 16) {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
        } else {
            val uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN
            window.decorView.systemUiVisibility = uiOptions
        }
    }

    protected fun showSecurityDialog(onSuccessCallBack: () -> Unit, onCancelCallBack: (() -> Unit)? = null) {
        val builder = AlertDialog.Builder(this)
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_security, null)
        val passwordEdit = dialogView.findViewById(R.id.password) as EditText
        builder.setTitle(getString(R.string.security_dialog_title))
                .setCancelable(false)
                .setView(dialogView)
                .setPositiveButton(getString(R.string.dialog_answer_ok), { dialog, id ->

                    val preferences = getSharedPreferences(Preferences.APP_SETTINGS.toString(), Context.MODE_PRIVATE)
                    val password = preferences.getString(Preferences.SECURITY_PASSWORD.toString(), null)
                    val typedPassword = passwordEdit.text.toString();

                    if (typedPassword.isNotEmpty()) {
                        if (typedPassword == password) {
                            onSuccessCallBack()
                        } else {
                            Toast.makeText(this, getString(R.string.incorrect_password_message), Toast.LENGTH_SHORT).show()
                            if (onCancelCallBack != null) {
                                onCancelCallBack()
                            }
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.password_required_message), Toast.LENGTH_SHORT).show()
                        if (onCancelCallBack != null) {
                            onCancelCallBack()
                        }
                    }

                    dialog.dismiss()
                })
                .setNegativeButton(getString(R.string.dialog_answer_cancel), { dialog, id ->
                    dialog.cancel()
                    if (onCancelCallBack != null) {
                        onCancelCallBack()
                    }
                }).create().show()
    }

    fun preventStatusBarExpansion(context: Context) {
        val manager = context.applicationContext
                .getSystemService(Context.WINDOW_SERVICE) as WindowManager

        val localLayoutParams = WindowManager.LayoutParams()
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR
        localLayoutParams.gravity = Gravity.TOP
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or
                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or
                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN or WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        //https://stackoverflow.com/questions/1016896/get-screen-dimensions-in-pixels
        val resId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        var result = 0
        if (resId > 0) {
            result = context.resources.getDimensionPixelSize(resId)
        }

        localLayoutParams.height = result

        localLayoutParams.format = PixelFormat.TRANSPARENT

        if (WindowViewTracker.currentView == null) {
            WindowViewTracker.currentView = StatusBarBlockerView(context)
            manager.addView(WindowViewTracker.currentView, localLayoutParams)
        }
    }

    fun allowStatusBarExpansion(context: Context) {
        val manager = context.applicationContext
                .getSystemService(Context.WINDOW_SERVICE) as WindowManager

        if (WindowViewTracker.currentView != null) {
            manager.removeView(WindowViewTracker.currentView)
            WindowViewTracker.currentView = null
        }
    }

}

class StatusBarBlockerView(context: Context) : ViewGroup(context) {

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {}

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return true
    }
}