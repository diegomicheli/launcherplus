package movilplus.com.launcherplus.preferences

enum class Preferences {
    APP_SETTINGS,
    SELECTED_APPS,
    SECURITY_PASSWORD,
    LOGO,
}