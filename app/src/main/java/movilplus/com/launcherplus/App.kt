package movilplus.com.launcherplus

import android.app.Application
import android.content.Context
import android.content.Intent
import movilplus.com.launcherplus.preferences.Preferences
import movilplus.com.launcherplus.services.SecurityService

class App : Application() {

    val DEFAULT_PASSWORD = "123456"

    override fun onCreate() {
        super.onCreate()

        val preferences = getSharedPreferences(Preferences.APP_SETTINGS.toString(), Context.MODE_PRIVATE)

        if (preferences.getString(Preferences.SECURITY_PASSWORD.toString(), "").isEmpty()) {
            val editor = preferences.edit()
            editor.putString(Preferences.SECURITY_PASSWORD.toString(), DEFAULT_PASSWORD)
            editor.commit()
        }

        startService(Intent(this, SecurityService::class.java))
    }
}